# ANC Study Registration

**This is a work in progress at an early trying-things-out stage.**

## Implementation

We implement the study registration survey as a static website using [SurveyJS](https://surveyjs.io/).

After completion, the survey results will be sent to a REST API for processing.

## Survey

We use [SurveyJS](https://surveyjs.io/) for implementing an rendering the survey.

## Security and authentication

The survey content is not classified, therefore it is open source.

The survey is hidden until the user authenticates. We use OAuth authentication authentication with authorization code and PKCE provided by our GitLab instance.

The authentication is implemented using the example:
- [article with example code and explanatory video](https://developer.okta.com/blog/2019/05/01/is-the-oauth-implicit-flow-dead)
- [code from the article](https://github.com/aaronpk/pkce-vanilla-js)

## Set up the application locally

1. Host the source under `http://localhost/studyreg/`.
2. [Add new application in GitLab.](https://docs.gitlab.com/ee/integration/oauth_provider.html)
3. Fill in the values in `config` variable in [`oauth-pkce.js`](oauth-pkce.js).
